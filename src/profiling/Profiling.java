/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package profiling;

import java.util.Scanner;

/**
 *
 * @author Karthik
 */
public class Profiling {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Enter order of square matrix, N = ");
        Scanner scan = new Scanner(System.in);
        int N = scan.nextInt();
        double[][] A = new double[N][N];
        double[][] B = new double[N][N];
        double[][] C;
        long start, stop;
        double elapsed1, elapsed2;
        start = System.currentTimeMillis();
        
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                A[i][j] = Math.random();

        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                B[i][j] = Math.random();
        stop = System.currentTimeMillis();
        elapsed1 = (stop - start);
        
        //row wise multiplication of matrices
        C = new double[N][N];
        start = System.currentTimeMillis(); 
        for (int i = 0; i < N; i++)
            for (int k = 0; k < N; k++)
                for (int j = 0; j < N; j++)
                    C[i][j] += A[i][j] * B[k][j];
        stop = System.currentTimeMillis();
        elapsed1 = (stop - start)/1000.0;
        System.out.println("Time to multiply two random matrices of order "+ N + " row-wise is: "+ elapsed1);
        
        //regular matrix multiplication
        C = new double[N][N];
        start = System.currentTimeMillis(); 
        for (int i = 0; i < N; i++)
            for (int k = 0; k < N; k++)
                for (int j = 0; j < N; j++)
                    C[i][j] += A[i][j] * B[j][k];
        stop = System.currentTimeMillis();
        elapsed2 = (stop - start)/1000.0;
        System.out.println("Time to multiply two random matrices of order "+ N + " column-wise is: "+ elapsed2);
        double diff = elapsed2 - elapsed1;
        System.out.println("Difference in response time " + diff);
     }
 }
